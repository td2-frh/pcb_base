EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:tp5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5100 3250 0    60   Input ~ 0
IN
Wire Wire Line
	5100 3250 5200 3250
$Comp
L R R3
U 1 1 57656A34
P 5350 3250
F 0 "R3" V 5430 3250 50  0000 C CNN
F 1 "10k" V 5350 3250 50  0000 C CNN
F 2 "" V 5280 3250 50  0001 C CNN
F 3 "" H 5350 3250 50  0000 C CNN
	1    5350 3250
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 57656A9F
P 5600 3450
F 0 "C5" H 5625 3550 50  0000 L CNN
F 1 "100nF" H 5625 3350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 5638 3300 50  0001 C CNN
F 3 "" H 5600 3450 50  0000 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
Text HLabel 5850 3250 2    60   Output ~ 0
OUT
Wire Wire Line
	5850 3250 5500 3250
Wire Wire Line
	5600 3300 5600 3250
Connection ~ 5600 3250
$Comp
L GNDA #PWR032
U 1 1 57656AF5
P 5600 3600
F 0 "#PWR032" H 5600 3350 50  0001 C CNN
F 1 "GNDA" H 5600 3450 50  0000 C CNN
F 2 "" H 5600 3600 50  0000 C CNN
F 3 "" H 5600 3600 50  0000 C CNN
	1    5600 3600
	1    0    0    -1  
$EndComp
Text Notes 4600 3100 0    60   ~ 0
TODO: Este filtro es muy básico! Mejorarlo.
$EndSCHEMATC
