EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:tp5-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5400 3950 0    60   Input ~ 0
IN
Wire Wire Line
	5400 3950 5500 3950
$Comp
L R R4
U 1 1 57657148
P 5650 3950
F 0 "R4" V 5730 3950 50  0000 C CNN
F 1 "10k" V 5650 3950 50  0000 C CNN
F 2 "" V 5580 3950 50  0001 C CNN
F 3 "" H 5650 3950 50  0000 C CNN
	1    5650 3950
	0    1    1    0   
$EndComp
$Comp
L C C6
U 1 1 5765714F
P 5900 4150
F 0 "C6" H 5925 4250 50  0000 L CNN
F 1 "100nF" H 5925 4050 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 5938 4000 50  0001 C CNN
F 3 "" H 5900 4150 50  0000 C CNN
	1    5900 4150
	1    0    0    -1  
$EndComp
Text HLabel 6150 3950 2    60   Output ~ 0
OUT
Wire Wire Line
	6150 3950 5800 3950
Wire Wire Line
	5900 4000 5900 3950
Connection ~ 5900 3950
$Comp
L GNDA #PWR033
U 1 1 5765715A
P 5900 4300
F 0 "#PWR033" H 5900 4050 50  0001 C CNN
F 1 "GNDA" H 5900 4150 50  0000 C CNN
F 2 "" H 5900 4300 50  0000 C CNN
F 3 "" H 5900 4300 50  0000 C CNN
	1    5900 4300
	1    0    0    -1  
$EndComp
Text Notes 4800 3850 0    60   ~ 0
TODO: Este filtro es muy básico! Mejorarlo.
$EndSCHEMATC
