EESchema Schematic File Version 2
LIBS:tp5-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lm2596
LIBS:LPC4337JBD144
LIBS:24lc64
LIBS:tp5-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LPC4337JBD144 U3
U 1 1 57657722
P 7600 6650
F 0 "U3" H 8600 3750 60  0000 C CNN
F 1 "LPC4337JBD144" H 8900 3650 60  0000 C CNN
F 2 "" H 7500 10600 60  0001 C CNN
F 3 "" H 7500 10600 60  0000 C CNN
	1    7600 6650
	1    0    0    -1  
$EndComp
Text HLabel 9800 7950 2    60   Output ~ 0
DAC_OUT
Wire Wire Line
	9800 7950 9350 7950
Text HLabel 9800 8050 2    60   Input ~ 0
ADC_IN
Wire Wire Line
	9800 8050 9350 8050
$Comp
L +3.3V #PWR021
U 1 1 57657CBF
P 7300 1100
F 0 "#PWR021" H 7300 950 50  0001 C CNN
F 1 "+3.3V" H 7300 1240 50  0000 C CNN
F 2 "" H 7300 1100 50  0000 C CNN
F 3 "" H 7300 1100 50  0000 C CNN
	1    7300 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 1100 7300 1600
Wire Wire Line
	7300 1500 8400 1500
Wire Wire Line
	8400 1500 8400 1600
Connection ~ 7300 1500
Wire Wire Line
	8300 1600 8300 1500
Connection ~ 8300 1500
Wire Wire Line
	8200 1600 8200 1500
Connection ~ 8200 1500
Wire Wire Line
	8100 1600 8100 1500
Connection ~ 8100 1500
Wire Wire Line
	8000 1600 8000 1500
Connection ~ 8000 1500
Wire Wire Line
	7900 1600 7900 1500
Connection ~ 7900 1500
Wire Wire Line
	7800 1600 7800 1500
Connection ~ 7800 1500
Wire Wire Line
	7700 1600 7700 1500
Connection ~ 7700 1500
Wire Wire Line
	7600 1600 7600 1500
Connection ~ 7600 1500
Wire Wire Line
	7500 1600 7500 1500
Connection ~ 7500 1500
Wire Wire Line
	7400 1600 7400 1500
Connection ~ 7400 1500
$Comp
L +3.3VADC #PWR022
U 1 1 57657A43
P 8650 1100
F 0 "#PWR022" H 8800 1050 50  0001 C CNN
F 1 "+3.3VADC" H 8650 1200 50  0000 C CNN
F 2 "" H 8650 1100 50  0000 C CNN
F 3 "" H 8650 1100 50  0000 C CNN
	1    8650 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 1600 8650 1100
$Comp
L GNDA #PWR023
U 1 1 57657AAC
P 8000 9950
F 0 "#PWR023" H 8000 9700 50  0001 C CNN
F 1 "GNDA" H 8000 9800 50  0000 C CNN
F 2 "" H 8000 9950 50  0000 C CNN
F 3 "" H 8000 9950 50  0000 C CNN
	1    8000 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 9800 8000 9950
Wire Wire Line
	8000 9900 8400 9900
Wire Wire Line
	8400 9900 8400 9800
Connection ~ 8000 9900
Wire Wire Line
	8200 9800 8200 9900
Connection ~ 8200 9900
$Comp
L GND #PWR024
U 1 1 57657B35
P 7600 9950
F 0 "#PWR024" H 7600 9700 50  0001 C CNN
F 1 "GND" H 7600 9800 50  0000 C CNN
F 2 "" H 7600 9950 50  0000 C CNN
F 3 "" H 7600 9950 50  0000 C CNN
	1    7600 9950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 9800 7600 9950
Wire Wire Line
	7600 9900 7900 9900
Wire Wire Line
	7900 9900 7900 9800
Connection ~ 7600 9900
Wire Wire Line
	7800 9800 7800 9900
Connection ~ 7800 9900
Wire Wire Line
	7700 9800 7700 9900
Connection ~ 7700 9900
$Comp
L +3.3VADC #PWR025
U 1 1 57656BD8
P 6300 8900
F 0 "#PWR025" H 6450 8850 50  0001 C CNN
F 1 "+3.3VADC" H 6300 9000 50  0000 C CNN
F 2 "" H 6300 8900 50  0000 C CNN
F 3 "" H 6300 8900 50  0000 C CNN
	1    6300 8900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 8900 6650 8900
Wire Wire Line
	6500 8900 6500 9000
Wire Wire Line
	6500 9000 6650 9000
Connection ~ 6500 8900
$Comp
L 24LC64-RESCUE-tp5 U4
U 1 1 576E91C8
P 11700 7250
AR Path="/576E91C8" Ref="U4"  Part="1" 
AR Path="/5765531B/576E91C8" Ref="U4"  Part="1" 
F 0 "U4" H 11950 7600 60  0000 C CNN
F 1 "24LC64" H 11800 6700 60  0000 C CNN
F 2 "" H 11700 7250 60  0001 C CNN
F 3 "" H 11700 7250 60  0000 C CNN
	1    11700 7250
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR026
U 1 1 576E92FF
P 12400 6950
F 0 "#PWR026" H 12400 6800 50  0001 C CNN
F 1 "+3.3V" H 12400 7090 50  0000 C CNN
F 2 "" H 12400 6950 50  0000 C CNN
F 3 "" H 12400 6950 50  0000 C CNN
	1    12400 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR027
U 1 1 576E937C
P 11100 7700
F 0 "#PWR027" H 11100 7450 50  0001 C CNN
F 1 "GND" H 11100 7550 50  0000 C CNN
F 2 "" H 11100 7700 50  0000 C CNN
F 3 "" H 11100 7700 50  0000 C CNN
	1    11100 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 7300 11100 7700
Wire Wire Line
	11100 7300 11200 7300
Wire Wire Line
	11200 7400 11100 7400
Connection ~ 11100 7400
Wire Wire Line
	11200 7500 11100 7500
Connection ~ 11100 7500
Wire Wire Line
	11200 7650 11100 7650
Connection ~ 11100 7650
$Comp
L GND #PWR028
U 1 1 576E950B
P 12200 7650
F 0 "#PWR028" H 12200 7400 50  0001 C CNN
F 1 "GND" H 12200 7500 50  0000 C CNN
F 2 "" H 12200 7650 50  0000 C CNN
F 3 "" H 12200 7650 50  0000 C CNN
	1    12200 7650
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 576E952E
P 12400 7250
F 0 "C4" H 12425 7350 50  0000 L CNN
F 1 "100nF" H 12425 7150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 12438 7100 50  0001 C CNN
F 3 "" H 12400 7250 50  0000 C CNN
	1    12400 7250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR029
U 1 1 576E9567
P 12400 7400
F 0 "#PWR029" H 12400 7150 50  0001 C CNN
F 1 "GND" H 12400 7250 50  0000 C CNN
F 2 "" H 12400 7400 50  0000 C CNN
F 3 "" H 12400 7400 50  0000 C CNN
	1    12400 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	12200 7050 12400 7050
Wire Wire Line
	12400 6950 12400 7100
Connection ~ 12400 7050
$Comp
L R R1
U 1 1 576E9802
P 9700 7050
F 0 "R1" V 9780 7050 50  0000 C CNN
F 1 "4.7k" V 9700 7050 50  0000 C CNN
F 2 "" V 9630 7050 50  0001 C CNN
F 3 "" H 9700 7050 50  0000 C CNN
	1    9700 7050
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 576E9856
P 9950 7050
F 0 "R2" V 10030 7050 50  0000 C CNN
F 1 "4.7k" V 9950 7050 50  0000 C CNN
F 2 "" V 9880 7050 50  0001 C CNN
F 3 "" H 9950 7050 50  0000 C CNN
	1    9950 7050
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR030
U 1 1 576E9881
P 9700 6900
F 0 "#PWR030" H 9700 6750 50  0001 C CNN
F 1 "+3.3V" H 9700 7040 50  0000 C CNN
F 2 "" H 9700 6900 50  0000 C CNN
F 3 "" H 9700 6900 50  0000 C CNN
	1    9700 6900
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR031
U 1 1 576E989B
P 9950 6900
F 0 "#PWR031" H 9950 6750 50  0001 C CNN
F 1 "+3.3V" H 9950 7040 50  0000 C CNN
F 2 "" H 9950 6900 50  0000 C CNN
F 3 "" H 9950 6900 50  0000 C CNN
	1    9950 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 7200 9700 7400
Wire Wire Line
	9950 7200 9950 7300
Text Label 9400 7300 0    60   ~ 0
SCL
Text Label 9400 7400 0    60   ~ 0
SDA
Wire Wire Line
	9950 7300 9350 7300
Wire Wire Line
	9700 7400 9350 7400
Text Label 10950 7150 0    60   ~ 0
SCL
Wire Wire Line
	10950 7150 11200 7150
Text Label 10950 7050 0    60   ~ 0
SDA
Wire Wire Line
	10950 7050 11200 7050
NoConn ~ 9350 2000
NoConn ~ 9350 2100
NoConn ~ 9350 2200
NoConn ~ 9350 2300
NoConn ~ 9350 2400
NoConn ~ 9350 2500
NoConn ~ 9350 2600
NoConn ~ 9350 2700
NoConn ~ 9350 2850
$EndSCHEMATC
